import React from 'react';
import Board from './Board';
import { calculateWinner } from '../utils';

class Game extends React.Component {
    handleClick = (i) => {
      const { history: oldHistory, stepNumber, clickSquare } = this.props;
      const history = oldHistory.slice(0, stepNumber + 1);
      const current = history[history.length - 1];
      const squares = current.squares.slice();

      if (calculateWinner(squares) || (squares[i])) {
        return;
      }

      clickSquare(i);
    };

    render() {
      const { stepNumber, jumpTo, history, xIsNext } = this.props;
      const current = history[stepNumber];
      const winner = calculateWinner(current.squares);

      const moves = history.map((step, move) => {
        const desc = move ? `Go to move #${move}` : 'Go to game start';

        return (
          <li key={move}>
            <button onClick={() => jumpTo(move)}>{desc}</button>
          </li>
        );
      });

      const status = winner ? `Winner is: ${winner}` : `Next player ${xIsNext ? 'X' : 'o'}`

      return (
        <div className="game">
          <div className="game-board">
            <Board
              squares={current.squares}
              onClick={(i) => this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
      );
    }
}

export default Game;