import React from 'react';
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom';
import store from './redux/store';
import GameContainer from './containers/GameContainer';
import './index.css';
  
ReactDOM.render(
  <Provider store={store}>
    <GameContainer />
  </Provider>,
  document.getElementById('root')
);