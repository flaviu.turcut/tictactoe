import { connect } from 'react-redux';
import Game from '../components/Game';
import { jumpTo, clickSquare } from '../redux/actions';
import { getStepNumber, getHistory } from '../redux/selectors';

const mapStateToProps = (state) => ({
    stepNumber: getStepNumber(state),
    history: getHistory(state),
});

const mapDispatchToProps = (dispatch) => ({
    jumpTo: (stepNumber) => {
        dispatch(jumpTo(stepNumber));
    },
    clickSquare: (i) => {
        dispatch(clickSquare(i));
    },
});

const GameContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Game);

export default GameContainer;
