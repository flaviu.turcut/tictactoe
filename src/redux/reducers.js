import { Record } from 'immutable';
import { JUMP_TO, CLICK_SQUARE } from './actionTypes';
import { calculateWinner } from '../utils';

const Game = new Record({
    history: [{
        squares: Array(9).fill(null),
    }],
    xIsNext: true,
    stepNumber: 0,
});

export default (state = new Game, action) => {
    switch (action.type) {
        case JUMP_TO:
            const { stepNumber } = action.payload;

            return state.merge({
                stepNumber,
            });
        case CLICK_SQUARE:
                const { i } = action.payload;
                const { history: oldHistory, xIsNext, stepNumber: oldStepNumber } = state;
                const history = oldHistory.slice(0, oldStepNumber + 1);
                const current = history[history.length - 1];
                const squares = current.squares.slice();
                squares[i] = xIsNext ? 'X' : 'O';

                return state.merge({
                    history: history.concat([{
                        squares: squares,
                    }]),
                    stepNumber: history.length,
                    xIsNext: !xIsNext,
                });
        default:
            break;
    }
};