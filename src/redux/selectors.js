export const getStepNumber = store => store ? store.stepNumber : 0;
export const getHistory = store => store ? store.history : [{ squares: Array(9).fill(null) }];