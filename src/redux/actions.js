import { JUMP_TO, CLICK_SQUARE } from './actionTypes';

export const jumpTo = stepNumber => ({
    type: JUMP_TO,
    payload: { stepNumber },
});

export const clickSquare = i => ({
    type: CLICK_SQUARE,
    payload: { i },
});